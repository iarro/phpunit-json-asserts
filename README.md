# PHPUnit JSON Asserts

This library adds some assertions to PHPUnit that allow you to check JSON data.

## Usage
If you want to compare two JSON objects, simply use `JsonSubsetAssert`.

```php
<?php
use Iarro\Assert\JsonSubsetAssert;
use PHPUnit\Framework\TestCase;

class MyTestCase extends TestCase
{
    use JsonSubsetAssert;

    public function testJsonDocument()
    {
        $this->assertJsonSubset('{"message": "hello", "number": 1}', '{"number": 1}');
    }
}

```
