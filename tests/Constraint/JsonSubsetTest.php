<?php
declare(strict_types=1);

namespace Iarro\Tests\Constraint;

use Iarro\Constraint\JsonSubset;
use PHPUnit\Framework\TestCase;

class JsonSubsetTest extends TestCase
{
    public static function evaluateDataProvider(): array
    {
        return [
            'simple array actual' => [
                'result' => true,
                'actual' => '["a", "b", "c"]',
                'expected' => '["a", "b", "c", "d"]',
            ],
            'array subset with moved order of items' => [
                'result' => true,
                'actual' => '["b", "c"]',
                'expected' => '["a", "b", "c", "d"]',
            ],
            'array subset with different order of items' => [
                'result' => true,
                'actual' => '["a", "c", "b"]',
                'expected' => '["a", "b", "c", "d"]',
            ],
            'array subset with unexpexted item' => [
                'result' => false,
                'actual' => '["a", "c", "unresult"]',
                'expected' => '["a", "b", "c", "d"]',
            ],
            'object if array result' => [
                'result' => false,
                'actual' => '{"a": "a", "b":"b", "c":"c"}',
                'expected' => '["a", "b", "c", "d"]',
            ],
            'simple object actual' => [
                'result' => true,
                'actual' => '{"a": "a", "b":"b", "c":"c"}',
                'expected' => '{"a": "a", "b":"b", "c":"c", "d":"d"}',
            ],
            'object subset with moved order of properties' => [
                'result' => true,
                'actual' => '{"b":"b", "c":"c"}',
                'expected' => '{"a": "a", "b":"b", "c":"c", "d":"d"}',
            ],
            'object subset with different order of properties' => [
                'result' => true,
                'actual' => '{"a": "a", "c":"c", "b":"b"}',
                'expected' => '{"a": "a", "b":"b", "c":"c", "d":"d"}',
            ],
            'object subset with unexpexted property' => [
                'result' => false,
                'actual' => '{"a": "a", "c":"c", "unresult":"c"}',
                'expected' => '{"a": "a", "b":"b", "c":"c", "d":"d"}',
            ],
            'numeric property' => [
                'result' => false,
                'actual' => '{"int": "10"}',
                'expected' => '{"int": 10}',
            ],
            'empty object property' => [
                'result' => false,
                'actual' => '{"empty": ""}',
                'expected' => '{"empty": null}',
            ],
            'property with empty object or array' => [
                'result' => false,
                'actual' => '{"obj": []}',
                'expected' => '{"obj": {}}',
            ],
            'property with sub object or array' => [
                'result' => false,
                'actual' => '{"obj": ["hello"]}',
                'expected' => '{"obj": {"hello": "dolly"}}',
            ],
            'mixed objects and arrays' => [
                'result' => false,
                'actual' => '{"obj": ["a", {"b": "c"}, [3, 4]]}',
                'expected' => '{"obj": [{"b": "value"}, "a", [1, 2]]}',
            ],
            'object with arrays and other objects' => [
                'result' => false,
                'actual' => '{"obj": ["a", "b", {"c": "value", "d": [1, 2, null]}]}',
                'expected' => '{"obj": ["a", "b", {"c": "value", "d": [1, 2, 3]}]}',
            ],
            'mixed result arrays' => [
                'result' => true,
                'actual' => '[[1, 2], [1, 2]]',
                'expected' => '[[1, 2], [1, 2], [3, 4]]',
            ],
            'mixed unresult arrays' => [
                'result' => false,
                'actual' => '[[1, 2], [1, 2]]',
                'expected' => '[[1, 2]]',
            ],
        ];
    }

    /**
     * @param bool $result
     * @param string $actual
     * @param string $expected
     *
     * @dataProvider evaluateDataProvider
     */
    public function testEvaluate(bool $result, string $actual, string $expected): void
    {
        $constraint = new JsonSubset($expected);

        $this->assertSame($result, $constraint->evaluate($actual, '', true));
    }
}
