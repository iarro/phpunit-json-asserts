<?php
declare(strict_types=1);

namespace Iarro\Constraint;

use Iarro\Exception\InvalidJsonException;

class JsonSubset extends \PHPUnit\Framework\Constraint\Constraint
{
    /**
     * @var string
     */
    private $expected;

    public function __construct(string $expected)
    {
        $this->expected = $expected;
    }

    /**
     * {@inheritdoc}
     */
    protected function matches($actual): bool
    {
        if (!is_string($actual)) {
            throw new InvalidJsonException($actual);
        }

        $objectActual = json_decode($actual);
        if (json_last_error()) {
            throw new InvalidJsonException($actual);
        }

        $objectExpected = json_decode($this->expected);
        if (json_last_error()) {
            throw new InvalidJsonException($this->expected);
        }

        if (gettype($objectExpected) !== gettype($objectActual)) {
            return false;
        }

        if (is_array($objectExpected)) {
            return $this->matchesArrays($objectExpected, $objectActual);
        }

        if (is_object($objectExpected)) {
            return $this->matchesObject($objectExpected, $objectActual);
        }

        return $objectExpected === $objectActual;
    }

    /**
     * {@inheritdoc}
     */
    public function toString(): string
    {
        return 'has the subset '.$this->exporter()->export($this->expected);
    }

    /**
     * {@inheritdoc}
     */
    protected function failureDescription($other): string
    {
        return 'an JSON '.$this->toString();
    }

    private function matchesArrays(array $expected, array $actual): bool
    {
        foreach ($actual as $actValue) {
            foreach ($expected as $k => $expValue) {
                if (is_object($actValue) && is_object($expValue) && false !== $this->matchesObject($expValue, $actValue)) {
                    // we want to prevent comparing same value repeatly, so we removed it
                    unset($expected[$k]);
                    continue 2;
                }

                if (is_array($actValue) && is_array($expValue) && false !== $this->matchesArrays($expValue, $actValue)) {
                    // we want to prevent comparing same value repeatly, so we removed it
                    unset($expected[$k]);
                    continue 2;
                }

                if ($actValue === $expValue) {
                    // we want to prevent comparing same value repeatly, so we removed it
                    unset($expected[$k]);
                    continue 2;
                }
            }

            return false;
        }

        return true;
    }

    private function matchesObject(object $expected, object $actual): bool
    {
        foreach ($actual as $property => $value) {
            if (!property_exists($expected, $property)) {
                return false;
            }

            if (gettype($value) !== gettype($expected->$property)) {
                return false;
            }

            if (is_array($value) && false === $this->matchesArrays($expected->$property, $value)) {
                return false;
            }

            if (is_object($value) && false === $this->matchesObject($expected->$property, $value)) {
                return false;
            }

            if ($value !== $expected->$property) {
                return false;
            }
        }

        return true;
    }
}
