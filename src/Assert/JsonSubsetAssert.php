<?php
declare(strict_types=1);

namespace Iarro\Assert;

use Iarro\Constraint\JsonSubset;
use PHPUnit\Framework\Assert;

trait JsonSubsetAssert
{
    /**
     * Assert that actual JSON is fully contained in expected JSON.
     *
     * @param string $expected
     * @param string $actual
     * @param string $message
     */
    public function assertJsonSubset(string $expected, string $actual, string $message = ''): void
    {
        Assert::assertThat($actual, new JsonSubset($expected), $message);
    }
}
