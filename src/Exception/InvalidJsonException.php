<?php
declare(strict_types=1);

namespace Iarro\Exception;

class InvalidJsonException extends \PHPUnit\Framework\Exception
{
    public function __construct($value)
    {
        if (!is_scalar($value)) {
            $value = gettype($value);
        }

        parent::__construct(sprintf('%s is not valid JSON', $value));
    }
}
